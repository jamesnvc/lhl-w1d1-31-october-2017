//
//  Room.m
//  OOPDemo
//
//  Created by James Cash on 31-10-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Room.h"

@implementation Room

- (instancetype)init {
    self = [super init];
    if (self) {
        _height = 10;
        _length = 10;
        _depth = 5;
    }
    return self;
}

- (instancetype)initWithHeight:(NSInteger)height
                         depth:(NSInteger)depth
                        length:(NSInteger)length
{
    self = [super init];
    if (self) {
        _height = height;
        _depth = depth;
        _length = length;
    }
    return self;
}

- (NSInteger)volume {
    return self.height * self.depth * self.length;
}

@end
