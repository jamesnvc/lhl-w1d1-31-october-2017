//
//  Room.h
//  OOPDemo
//
//  Created by James Cash on 31-10-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Room : NSObject

@property (nonatomic,assign) NSInteger height;
@property (nonatomic,assign) NSInteger depth;
@property (nonatomic,assign) NSInteger length;

- (instancetype)initWithHeight:(NSInteger)height depth:(NSInteger)depth length:(NSInteger)length;

- (NSInteger)volume;

@end
