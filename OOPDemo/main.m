//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 31-10-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Room.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        /*
        int room1Length = 10;
        int room1Depth = 15;
        int room1Height = 3;
        NSLog(@"Room 1 volume = %d", room1Length * room1Depth * room1Height);
         */
//        Room* room1 = [[Room alloc] init];
        Room *room1 = [[Room alloc] initWithHeight:3 depth:15 length:10];
//        room1.length = 10;
//        room1.depth = 15;
//        room1.height = 3;
//        NSLog(@"Room 1 volume = %ld", room1.length * room1.depth * room1.height);
        NSLog(@"Room 1 volume = %ld", [room1 volume]); //room1.volume

        /*
        int room2Length = 5;
        int room2Depth = 10;
        int room2Height = 4;
        NSLog(@"Room 2 volume = %d", room2Length * room2Depth * room2Height);
*/
        Room *room2 = [[Room alloc] init];
        room2.length = 5;
        room2.depth = 10;
        room2.height = 4;
        NSLog(@"Room 2 volume = %ld", [room2 volume]);

        NSArray *rooms = @[room1, room2, [[Room alloc] init]];
        Room *room3 = rooms[2];
        for (Room *room in rooms) {
            NSLog(@"Room of volume %ld", room.volume);
        }
    }
    return 0;
}
